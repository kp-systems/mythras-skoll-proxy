from functools import cached_property
from typing import List

import requests


class SkollServer:
    base_url: str = 'https://skoll.xyz/mythras_eg/'
    @cached_property
    def enemy_template_list(self):
        skoll_response = requests.get(f'{self.base_url}index_json/')
        templates_list: List = skoll_response.json()
        templates_list.sort(key = lambda template : template['id'])
        return templates_list

    @cached_property
    def party_template_list(self):
        skoll_response = requests.get(f'{self.base_url}party_index_json/')
        templates_list: List = skoll_response.json()
        templates_list.sort(key = lambda template : template['id'])
        return templates_list

    def reset_template_list_cache(self):
        del self.enemy_template_list
        del self.party_template_list

    def get_skoll_json(self, endpoint: str, id: str):
        if not id:
            raise ValueError('Please include the id for the enemy you would like to generate')
        skoll_response = requests.get(f'{self.base_url}{endpoint}/?id={id}')
        try:
            skoll_json = skoll_response.json()
            return skoll_json
        except ValueError as err:
            raise ValueError('The id you entered could not be found. Please try a different id') from err