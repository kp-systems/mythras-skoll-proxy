from flask import Flask, jsonify, request

from app.skoll_server import SkollServer

def create_server():
    app = Flask(__name__)

    skoll_server = SkollServer()

    @app.route("/generate_enemy_json")
    def generate_enemies_json():
        try:
            id = request.args.get('id')
            return format_cors_json_response(skoll_server.get_skoll_json('generate_enemies_json', id))
        except ValueError as err:
            return str(err), 400

    @app.route("/generate_party_json")
    def generate_party_json():
        try:
            id = request.args.get('id')
            return format_cors_json_response(skoll_server.get_skoll_json('generate_party_json', id))
        except ValueError as err:
            app.logger.info('Error occurred when parsing skoll json response', exc_info=err)
            return str(err), 400

    @app.route("/get_enemy_template_list")
    def get_enemy_template_list():
        return format_cors_json_response(skoll_server.enemy_template_list)

    @app.route("/get_party_template_list")
    def get_party_template_list():
        return format_cors_json_response(skoll_server.party_template_list)

    def format_cors_json_response(json):
        response = jsonify(json)
        response.headers.add("Access-Control-Allow-Origin", "*")
        return response

    return app
