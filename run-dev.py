from app import create_server

def run():
    create_server().run('127.0.0.1', 8080, debug=True)

if __name__ == '__main__':
    run()